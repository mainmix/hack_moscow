var http = require('http');

const master = process.argv[2] == '--master=true';
const port = parseInt(process.argv[3].split('=')[1]);


// server side, reacts to requests
http.createServer(function (req, res) {
    if (master) {
        return requestMasterMapper(req, res);
    } else {
        return requestSlaveMaster(req, res);
    }
}).listen(port);


// node: { name: string, host: string, status: string }
let nodesMap = [];


const requestMasterMapper = (req, res) => {
    const { url } = req;
    switch (url) {
        case '/alive':
            const newNode = {
                name: `node-${nodesMap.length + 1}`,
                host: req.headers.host,
                status: 'alive'
            };
            nodesMap = [...nodesMap.filter(n => n.host === newNode), newNode];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({ status: "ok" }));
            res.end();
            return;
        case '/get-nodes':
            if (true) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify(nodesMap));
                res.end();
            }
            return;
    }
};

const requestSlaveMaster = (req, res) => {
    const { url } = req;
    switch (url) {
        case '/ask':
            let x = null;
            req.on('data', chunk => {
                x += chunk;
            });
            req.on('end', () => {
                console.log(res);
                var fs = require('fs');
                var uuid = require('uuid').v4();
                fs.writeFileSync(__dirname + '/uploads/' + uuid, x);
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({jobId: uuid}));
                res.end();
            });
            return;

        case '/response':
            let body = '';
            req.on('data', chunk => {
                body += chunk.toString();
            });
            req.on('end', () => {
                const parsed = JSON.parse(body);
                const { job } = parsed;
                var fs = require('fs');
                fs.writeFileSync(__dirname + '/responses/' + job, 'ok');
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({status: "ok"}));
                res.end();
            });
            return;
    }
};

const MASTER_HOST = {
    hostname: 'localhost',
    port: 8000
};

const makeBecomeAliveRequest = () => {
    const options = {
        hostname: MASTER_HOST.hostname,
        port: MASTER_HOST.port,
        path: '/alive',
        method: 'POST'
    };

    const req = http.request(options, res => {
        console.log(`Making request for alive: ${res.statusCode}`)
        res.on('data', (data) => {
            const normalized = data.toString();
            console.log(`Receiving response from alive: ${normalized}`);
        })
    });

    req.on('error', error => {
        console.error(error)
    });

    req.end();
};

const makeGetNodesList = () => {
    const options = {
        hostname: MASTER_HOST.hostname,
        port: MASTER_HOST.port,
        path: '/get-nodes',
        method: 'GET'
    };

    const req = http.request(options, res => {
        console.log(`Making request for get-nodes: ${res.statusCode}`)
        res.on('data', (data) => {
            const normalized = data.toString();
            console.log(`Receiving response from get-nodes: ${normalized}`);
        })
    });

    req.on('error', error => {
        console.error(error)
    });

    req.end();
};

!master && makeBecomeAliveRequest();
!master && makeGetNodesList();




