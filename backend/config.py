master = {
    'token_map':
        {
            '13.68.177.175:80': 'jewhgvtkqju3tvhl',
            '40.70.247.39:80': '9a8w7be9vabv9baw',
            '13.72.68.188:80': 'avuiawuvaobwvuo8',
        }
}

slaves = [
    {
        'token': 'jewhgvtkqju3tvhl',
        'address': '13.68.177.175:80',
        'name': 'Amgen Inc.',
    },
    {
        'token': '9a8w7be9vabv9baw',
        'address': '40.70.247.39:80',
        'name': 'Gilead Sciences',
    },
    {
        'token': 'avuiawuvaobwvuo8',
        'address': '13.72.68.188:80',
        'name': 'Celgene Corporation',
    },
]

max_log_len = 2000

