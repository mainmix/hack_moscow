# from interlocator; implements logic of endpoints
# also here we create requests
from .Interlocutor import Interlocutor


class MasterNode(Interlocutor):
    def __init__(self, token_map):
        super(MasterNode, self).__init__()
        self.token_map = token_map
        self.nodes = []  # each elem is a list [token, address, name, status]
        self.logs = []

