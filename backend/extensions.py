import json
import os
import pika
from datetime import datetime
import subprocess
import time
import threading

from . import config
from .keys import read_key
from .SlaveNode import SlaveNode
from .MasterNode import MasterNode
from .config import max_log_len

master_node = os.environ['XGEN_MODE'] == 'master'


def create_node(master_node):
    if not master_node:
        curr_node = SlaveNode(
            os.environ['ADDRESS'],
            os.environ['NAME'],
            os.environ['TOKEN'],
            os.environ['MASTER_ADDRESS']
        )
        curr_node.alive()
    else:
        curr_node = MasterNode(config.master['token_map'])
        for slave in config.slaves:
            curr_node.nodes.append([
                slave['token'], slave['address'], slave['name'], SlaveNode.DEAD
            ])

    return curr_node


node = create_node(master_node)


def log(obj, action, message):
    assert isinstance(node, MasterNode)
    dt = datetime.now()
    dt = dt.strftime('%d/%m/%Y %H:%M:%S')
    if len(node.logs) == max_log_len:
        node.logs.pop(0)
    node.logs.append({'dt': dt, 'obj': obj, 'action': action, 'message': message})

while True:
    try:
        time.sleep(1)
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq', port=5672))
        print(123)
        break
    except Exception as e:
        print(e)
        print(type(e))


JOBS_QUEUE = 'jobs_queue'
push_channel = connection.channel()

push_channel.queue_declare(queue=JOBS_QUEUE)
