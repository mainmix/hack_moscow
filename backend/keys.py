import rsa


def read_key(filename, key_type='public'):
    with open(filename, 'rb') as f:
        prv = f.read()
    key_cls = rsa.PublicKey if key_type == 'public' else rsa.PrivateKey
    return key_cls.load_pkcs1(prv)