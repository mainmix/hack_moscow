#!/usr/bin/env python
import subprocess
import time

import pika

JOBS_QUEUE = 'jobs_queue'
RESULTS_QUEUE = 'results_queue'

while True:
    try:
        time.sleep(1)
        connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
        print(123)
        break
    except Exception as e:
        print(type(e))
        print('rabbit not started yet')
channel = connection.channel()

channel.queue_declare(queue=JOBS_QUEUE)
channel.queue_declare(queue=RESULTS_QUEUE)


def process(ch, method, properties, body):
    # job = subprocess.Popen('stress -c 1 -t 10')
    time.sleep(10)
    # job.kill()
    print(" [x] Received %r" % body)
    res_channel = connection.channel()
    res_channel.queue_declare(queue=RESULTS_QUEUE)
    channel.basic_publish(exchange='',
                          routing_key=RESULTS_QUEUE,
                          body=body)
    print('pushed to results_queue')
    # publish result here


channel.basic_consume(queue=JOBS_QUEUE,
                      auto_ack=True,
                      on_message_callback=process)
channel.start_consuming()
