
from uuid import uuid4
import hashlib

from flask import Blueprint, request, jsonify, make_response
from . import config
from .extensions import node, log
from .SlaveNode import SlaveNode

blueprint = Blueprint('master_node', __name__, url_prefix='/master_node')


@blueprint.route('/test', methods=['GET'])
def test():
    return 'test_msg'


@blueprint.route('/alive', methods=['POST'])
def alive():
    from .extensions import node
    # encrypted_message = request.data['message']
    # message = rsa.decrypt(encrypted_message, master.prv).decode('utf-8')
    base_message = request.json.get('base_str')
    encrypted_message = request.json.get('encrypted_str')
    host = request.headers.get('Remote-Addr')
    node_token = node.token_map[host]
    for n in node.nodes:
        if n[1] == host:
            break
    else:
        log('Controller', 'Validate', 'Unknown node from %r asked for validation' % host)
        return jsonify({'status': 'FAIL'})

    result_str = base_message + node_token
    encrypted_message2 = hashlib.md5(result_str.encode('utf-8')).hexdigest()
    if encrypted_message == encrypted_message2:
        n[-1] = SlaveNode.ALIVE
        log(n[2], 'Validate', 'Node successfully validated')
        return jsonify({'status': 'OK'})
    else:
        node.nodes.remove(n)
        log(n[2], 'Validate', 'Node failed the validation. Deleted')
        return jsonify({'status': 'FAIL'})


@blueprint.route('/nodes_list', methods=['GET'])
def get_nodes_list():
    from .extensions import node
    nodes = []
    for i in node.nodes:
        token, address, name, status = i
        node = {
            'address': address,
            'name': name,
            'token': token,
            'status': 'alive' if status == SlaveNode.ALIVE else 'dead'
        }
        nodes.append(node)

    log('Controller', 'Get Nodes List', 'Controller returned %d nodes' % len(nodes))
    return jsonify(nodes)


@blueprint.route('/log', methods=['POST'])
def log_it():
    message = request.json.get('message')
    if message is None:
        message = 'Controller got incorrect log message'
    invoker = request.json.get('invoker')
    action = request.json.get('action')
    log(invoker, action, message)
    return jsonify({'status': 'OK'})


@blueprint.route('/get_logs')
def get_logs():
    return jsonify(node.logs)


@blueprint.route('/revoke', methods=['POST'])
def revoke():
    address = request.json.get('address')
    for n in node.nodes:
        if n[1] == address:
            break
    else:
        log('Controller', 'Revoke', 'Node was not found')
        return jsonify({'status': 'FAIL'})

    node.nodes.remove(n)
    log('Controller', 'Revoke', 'Node at %r successfully revoked' % address)
    return jsonify({'status': 'OK'})
