#!/usr/bin/env python
import subprocess
import time
import json
import requests

import pika

JOBS_QUEUE = 'jobs_queue'
RESULTS_QUEUE = 'results_queue'

while True:
    try:
        time.sleep(1)
        connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
        print(123)
        break
    except:
        print('rabbit not started yet')
        time.sleep(3)
channel = connection.channel()

channel.queue_declare(queue=JOBS_QUEUE)
channel.queue_declare(queue=RESULTS_QUEUE)


def process(ch, method, properties, body):
    result = json.loads(body)
    print(f'got result message: {result}')
    status = result['status']
    job_uuid = result['job_uuid']
    address = result['address']
    payload = {
        'status': status,
        'job_uuid': job_uuid
    }
    r = requests.post(f'http://{address}/slave_node/response', json=payload)
    # publish result here


channel.basic_consume(queue=RESULTS_QUEUE,
                      auto_ack=True,
                      on_message_callback=process)
channel.start_consuming()
