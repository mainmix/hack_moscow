import time

import pika

JOBS_QUEUE = 'jobs_queue'
RESULTS_QUEUE = 'results_queue'

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue=JOBS_QUEUE)

counter = 0
# define callback and subscribe
while True:
    body = ('Job %d created' % counter)
    channel.basic_publish(exchange='',
                          routing_key=JOBS_QUEUE,
                          body=body.encode('utf-8'))
    print(body)
    time.sleep(10)
