# describe routes here
import rsa
from . import config
from .SlaveNode import SlaveNode
from .keys import read_key
import json
import threading

from uuid import uuid4
from flask import Blueprint, request, jsonify


from .extensions import node, push_channel, JOBS_QUEUE

blueprint = Blueprint('slave_node', __name__, url_prefix='/slave_node')


@blueprint.route('/ask', methods=['POST'])
def ask():
    all_nodes = [n for n in node.get_nodes_list() if n['status'] == 'alive']
    print(all_nodes)
    addr = request.headers.get('Remote-Addr')
    for n in all_nodes:
        if n['address'] == addr:
            break
    else:
        node.log_action('Ask', 'Request from unknown host(%s)' % addr)
        return jsonify('unknown host')

    if 'file' not in request.files:
        return jsonify('no_file'), 400
    file = request.files['file']
    # TODO Send file to worker
    node.log_action('Ask', 'Got file from %s' % n['name'])
    job_uuid = uuid4()
    response = {
        'status': 'initial',
        'job_uuid': job_uuid
    }
    status = {
        'status': 'done',
        'job_uuid': job_uuid.hex,
        'address': addr
    }
    push_channel.basic_publish(exchange='',
                               routing_key=JOBS_QUEUE,
                               body=json.dumps(status))
    print('published smth ot jobs queue')
    # t1 = threading.Thread(target=node.response, args=(status, ))
    # t1.start()
    # node.response(status)
    return jsonify(response)


@blueprint.route('/response', methods=['POST'])
def response():
    resp_json = request.json
    status = resp_json.get('status', 'something went wrong with status')
    job_uuid = resp_json.get('job_uuid', 'something went wrong with job_uuid')
    node.log_action('Response', f'Job with uuid: {job_uuid} is done. Status: {status}')
    return jsonify({'status': 'ok'})
