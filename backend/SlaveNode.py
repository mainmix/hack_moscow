import requests
import rsa
import json
import time
import threading
import hashlib
import uuid

from .Interlocutor import Interlocutor
from . import config


class SlaveNode(Interlocutor):
    DEAD = 0
    ALIVE = 1

    def __init__(self, address, name, token, master_address):
        super(SlaveNode, self).__init__()
        self.address = address
        self.name = name
        self.token = token
        self.status = SlaveNode.DEAD
        self.queue = []
        self.master_address = master_address

    # SLAVE to SLAVE
    def ask(self, file, addr):
        files = {'file': file}
        headers = {
            'remote_addr': self.address
        }
        resp = requests.post(f'http://{addr}/slave_node/ask', files=files, headers=headers)
        if resp.status_code == 400:
            self.log_action('Ask', 'Bad file')
        else:
            resp_json = resp.json()
            job_uuid = resp_json.get('job_uuid', 'job uuid error in ask client')
            status = resp_json.get('status', 'status error in ask client')
            self.log_action('Ask', 'Got job %r with status %r' % (job_uuid, status))

    def response(self, status):
        addr = status.get('address', 'address error in response client')  # TODO change in future
        job_uuid = status.get('job_uuid', 'job uuid error in response client')
        payload = {
            'status': status,
            'job_uuid': job_uuid
        }
        r = requests.post(f'http://{addr}/slave_node/response', json=payload)
        if r.status_code != 200:
            self.log_action('Response', 'Node did not get successful result')

    # SLAVE to MASTER
    def alive(self):
        # message = config.vars['alive_message'].encode('utf-8')
        # encrypted = rsa.encrypt(message, self.master_pub)
        rand_str = uuid.uuid4()
        resstr = rand_str.hex + self.token
        encrypted_str = hashlib.md5(resstr.encode('utf-8'))
        r = self.client.post(
            'http://%s/%s' % (self.master_address, 'master_node/alive'),
            data=json.dumps({'encrypted_str': encrypted_str.hexdigest(), 'base_str': rand_str.hex}),
            headers={'content-type': 'application/json', 'remote_addr': self.address}
        )
        if r.status_code != 200:
            print(self.master_address, r.content)

        res = json.loads(r.content)
        status = res.get('status')
        if status == 'FAIL':
            print('Failed node validation')  # This is logged already

    def get_nodes_list(self):
        self.log_action('Get Nodes List', 'Node requested nodes list')
        r = self.client.get(f'http://{self.master_address}/master_node/nodes_list')
        return r.json()

    def log_action(self, action, message):
        self.client.post(
            f"http://{self.master_address}/master_node/log",
            headers={'content-type': 'application/json'},
            data=json.dumps({
                'invoker': self.name,
                'action': action,
                'message': message
            })
        )