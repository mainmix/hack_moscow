from flask import Blueprint, request, jsonify

from .extensions import node

blueprint = Blueprint('slave_control', __name__, url_prefix='/slave_control')


@blueprint.route('/send_file', methods=['get'])
def send_file():
    all_nodes = node.get_nodes_list()
    for u_node in all_nodes:
        if u_node['address'] != node.address:
            with open('./large_file', 'r') as file:
                node.log_action('Ask', 'Send file to %s' % u_node['name'])
                node.ask(file, u_node['address'])
    return jsonify(all_nodes)
