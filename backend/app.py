import decimal
import flask
import logging
import traceback
import os
from flask import Flask
from flask import jsonify
from uuid import UUID
import time
from flask_cors import CORS

from .MasterNodeListener import blueprint as master_node_blueprint
from .SlaveNodeListener import blueprint as slave_node_blueprint
from .slave_control import blueprint as control_blueprint
from . import config
from .keys import read_key
from .SlaveNode import SlaveNode
from .MasterNode import MasterNode

logging.basicConfig(level=logging.DEBUG)

companies = [{"id": 1, "name": "Company One"}, {"id": 2, "name": "Company Two"}]

master_node = os.environ['XGEN_MODE'] == 'master'


class MyJSONEncoder(flask.json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, decimal.Decimal):
            return str(obj)
        if isinstance(obj, UUID):
            return obj.hex
        return super(MyJSONEncoder, self).default(obj)


def create_app(cli=False):
    app = Flask('hack_moscow')
    CORS(app)
    register_blueprints(app, master_node)
    # node = create_node(master_node)

    app.json_encoder = MyJSONEncoder

    @app.errorhandler(Exception)
    def internal_error(exception):
        fingerprint = getattr(exception, 'fingerprint', None)
        print(traceback.format_exc())
        return jsonify({'msg': 'Server error', 'reason': traceback.format_exc()}), 500

    @app.errorhandler(404)
    def page_not_found(e):
        return jsonify({'msg': 'URL not found'}), 404

    return app


def register_blueprints(app, master_node):
    if master_node:  # TODO Replace with parameter
        app.register_blueprint(master_node_blueprint)
    else:
        app.register_blueprint(slave_node_blueprint)
        app.register_blueprint(control_blueprint)


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0')
