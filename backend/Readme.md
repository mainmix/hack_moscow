## Install
`conda create --name hackm python=3.7`

`conda init`

restart terminal

`conda acitvate hackm`

`cd backend`

`conda install -n hackm --file requirements.txt`

## Run
`export FLASK_APP="app:create_app"`

`flask run `

## Run slave node on port 8000
export XGEN_MODE=slave
flask run -p 8000

## Run master node on port 8001
export XGEN_MODE=master
flask run -p 8001


# WORKING WITH DOCKER 
in backend
`docker build -t base_node .`
There are some configs in backend/instances. Choose one and 
`docker run --name {slave_node} --env-file ./instances/{slave1} base_node`
where slave_node - the name of container, slave1 - config file

in order to remove existing container
docker rm {slave_node}