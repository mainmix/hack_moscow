import React from 'react';
import { SlaveNodes } from "./wrappers/slave-nodes/salve-nodes";
import {Journal} from "./wrappers/journal/journal";
import { Header } from './components/header';

import './styles/app.sass';

export class App extends React.Component {

  render() {
    return (
        <React.Fragment>
          <Header />
          <SlaveNodes/>
          <Journal />
        </React.Fragment>
    )
  }
}
