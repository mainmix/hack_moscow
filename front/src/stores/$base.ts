import {action, observable} from "mobx"
import * as NodeApi from "../api/nodes-api"

export interface INode {
    address: string
    name: string
    token: string
    status: 'alive' | 'dead' | 'unknown'
}

class _BaseStore {
    @observable nodes: INode[] = []
    @observable journal: string[] = []

    @action
    async loadNodes() {
        try {
            this.nodes = []
            const nodesList: INode[] = await NodeApi.loadNodes()
            this.nodes = nodesList
        } catch(e) {
            console.log('Cant load nodes')
            console.log(e)
        }
    }

    @action
    async loadJournal() {
        try {
            this.journal = []
            const journalRes: string[] = await NodeApi.loadJournal()
            this.journal = journalRes
        } catch(e) {
            console.log('Cant load journal')
            console.log(e)
        }
    }

    @action
    async revokeNode(address: string) {
        try {
            const ok = await NodeApi.revokeNode(address);
            if (ok) {
                this.loadNodes();
                this.loadJournal();
            }
        } catch(e) {
            console.log('Cant revoke node')
            console.log(e)
        }
    }
}

export const BaseStore = new _BaseStore();