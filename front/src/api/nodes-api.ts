import {INode} from "../stores/$base";
import axios from 'axios';

const MASTER_NODE = 'http://localhost:8000';

// NOTE: MOCK
// export const loadNodes: () => Promise<INode[]> = () => new Promise((resolve: any) => {
//     setTimeout(() => {
//         const nodes: INode[] = [
//             { title: 'node1', ip: '192.168.1.1', sshKey:'rsa-a3xp9rm83yrn3y', status: 'free' },
//             { title: 'node2', ip: '192.168.1.2', sshKey:'rsa-sdfser3rcq2ex3', status: 'free' },
//             { title: 'node3', ip: '192.168.1.3', sshKey:'rsa-ao3nrxtyi73btd', status: 'free' }
//         ]
//         resolve(nodes)
//     }, 2000)
// })

export const loadNodes: () => Promise<INode[]> = async () => {
    const res = await axios.get(`${MASTER_NODE}/master_node/get_nodes_list`);
    const { data } = res;

    return data;
};

// NOTE: MOCK
// export const revokeNode: (address: string) => Promise<boolean> = () => new Promise(resolve => {
//     setTimeout(() => {
//         resolve(true)
//     }, 1000)
// });
export const revokeNode: (address: string) => Promise<boolean> = async (address: string) => {
    const res = await axios.post(`${MASTER_NODE}/master_node/revoke`, { address });
    const { data } = res;
    const { status } = data;

   return status === 'OK';
};


export const loadJournal: () => Promise<string[]> = async () => {
    const res = await axios.get(`${MASTER_NODE}/master_node/get_logs`);
    const { data } = res;

    return data.map((line: { dt: string, obj: string, action: string, message: string }) => {
        const { dt, obj, action, message } = line;
        return `[${dt}] [${obj}] [${action}]: ${message}`;
    });
};

// export const loadJournal: () => Promise<string[]> = () => new Promise(resolve => {
//     setTimeout(() => {
//         resolve([
//             `[${(new Date(Date.now() - 3 * 1000)).toLocaleTimeString()}][node1][action=Login]: successful handshake`,
//             `[${(new Date(Date.now() - 4 * 1000)).toLocaleTimeString()}][node1][action=GetList]: fetching nodes listz`,
//             `[${(new Date(Date.now() - 5 * 1000)).toLocaleTimeString()}][node2][action=Login]: successful handshake`,
//             `[${(new Date(Date.now() - 6 * 1000)).toLocaleTimeString()}][node2][action=GetList]: fetching nodes list`,
//             `[${(new Date(Date.now() - 7 * 1000)).toLocaleTimeString()}][node3][action=Login]: successful handshake`,
//             `[${(new Date(Date.now() - 8 * 1000)).toLocaleTimeString()}][node3][action=GetList]: fetching nodes list`
//         ])
//     }, 1000)
// });