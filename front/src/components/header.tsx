import * as React from 'react'

import '../styles/header.sass'

export class Header extends React.Component {
    constructor(props: any) {
        super(props)
    }

    render() {
        return (
            <div className="header__wrapper">
                <img src={`${process.env.PUBLIC_URL}/logo-gen-x.png`} />
            </div>
        )
    }
}