import * as React from 'react'
import {observer} from "mobx-react"
import { BaseStore } from "../../stores/$base"

import '../../styles/slave-nodes.sass'

@observer
export class Journal extends React.Component {
    constructor(props: any) {
        super(props);
        BaseStore.loadJournal();
    }

    render() {
        const { journal } = BaseStore;
        return (
            <React.Fragment>
                <a className="button button__reload" onClick={(e) => {
                    e.preventDefault();
                    BaseStore.loadJournal();
                }}>Reload</a>
                <div className="journal__wrapper">
                    {
                        journal.length
                            ? journal.map(r => <div className="journal__record">{ r }</div>)
                            : <img className="loader" src={`${process.env.PUBLIC_URL}/loader.svg`} />
                    }
                </div>
            </React.Fragment>
        )
    }
}