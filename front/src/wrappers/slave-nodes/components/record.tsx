import * as React from 'react';
import { INode } from "../../../stores/$base";
import { BaseStore } from "../../../stores/$base";

import '../../../styles/slave-nodes.sass';

interface IProps {
    node: INode
}

const Status = (status: 'alive' | 'dead' | 'unknown') => (
    <div className={`status status--${status}`} />
);

export class Record extends React.Component<IProps> {
    render() {
        const { node } = this.props;
        return (
            <div className="row__wrapper">
                <div className="row__col row__col--title">{node.name}</div>
                <div className="row__col row__col--ip">{node.address}</div>
                <div className="row__col row__col--status">{Status(node.status)}</div>
                <div className="row__col row__col--actions">
                    <a href="#" onClick={(e) => {
                    e.preventDefault();
                    BaseStore.revokeNode(node.address);
                    }}
                    >[revoke]</a>
                </div>
            </div>
        )
    }
}