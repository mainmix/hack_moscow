import * as React from 'react'
import {observer} from "mobx-react";

import { BaseStore } from '../../stores/$base';

import { Record } from "./components/record";

import '../../styles/slave-nodes.sass';


@observer
export class SlaveNodes extends React.Component {
 constructor(props: any) {
     super(props)
     BaseStore.loadNodes()
 }
 render() {
     const { nodes } = BaseStore;
     return (
         <React.Fragment>
             <a className="button button__reload" onClick={(e) => {
                 e.preventDefault();
                 BaseStore.loadNodes();
             }}>Reload</a>
             <div className="slave-nodes__wrapper" style={{ marginBottom: 20 }}>
                 {
                     nodes.length
                         ? (
                            <React.Fragment>
                                <div className="row__wrapper row__wrapper--th">
                                    <div className="row__col row__col--title">Title</div>
                                    <div className="row__col row__col--ip">Address</div>
                                    <div className="row__col row__col--status">Status</div>
                                    <div className="row__col row__col--actions">Actions</div>
                                </div>
                                {
                                    nodes.map(n => <Record node={n} />)
                                }
                            </React.Fragment>
                         )
                         : (
                             <img className="loader" src={`${process.env.PUBLIC_URL}/loader.svg`} />
                         )
                 }
             </div>
         </React.Fragment>
     )
 }
}